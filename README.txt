---- Installing Vadi-Mapper ----

These instructions are tested and work on Ubuntu 10.04 64-bit:


If you just wish to run/use Mudbot and the mapper, unpack the mapper and vadi_mapper.so to it's own directory. 

Navigate to the directory you unpacked it in, using the terminal then just execute:

    chmod u+x mapper
    ./mapper


---- Compiling ----

First, add the jansson ppa:
    sudo apt-add-repository ppa:petri/ppa && sudo apt-get update
    
Then install the needed libraries:
    sudo apt-get install libjansson0 bzr bzr-gtk build-essential libcurl4-gnutls-dev libglib2.0-dev libjson-glib-dev


If all goes well, run the following command:

    sh make-mapper-linux.sh'
    
In order to UPDATE the sources for the vadi_mapper:
    http://vadisystems.com/vadi-mapper/index.php/Compiling
    
    mkdir ~/Programs && cd ~/Programs
    bzr branch http://bazaar.launchpad.net/~vadi-mapper-dev/vadi-mapper/main vadi-mapper

    To to compile the first time or recompile the mapper later on (like after an update), do this:

    cd ~/Programs/vadi-mapper
    make

    Then simply move the resulting vadi_mapper.so file into the systems folder, and do `reload Vadi Mapper.

Be sure that you save the source in a DIFFERENT directory, then copy the contents of THIS archive into the directory you saved the source in. Then re-compile everything and you should have an updated module version.
