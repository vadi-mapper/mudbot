# Generic Makefile.

# Export BUILDTYPE as: nothing, or "dist"

# Files to be built.
SO_FILES = vadi_mapper.so

# Target Operating System: Linux, Darwin, or SunOS.
OS = $(shell uname)

# Header and Library directories.
INC = -I./deps/$(OS)/ -I.
LIB = -L./deps/$(OS)/ -L.

# Defaults.
# C_FLAGS go to gcc. B_FLAGS for binaries, M_FLAGS for libraries/modules.
CC      = gcc
C_FLAGS = $(INC) $(LIB) 
#-D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror
B_FLAGS = 
M_FLAGS = 

#ifeq ($(BUILDTYPE),dist)
#  C_FLAGS += -O3 -Wall
#else
  C_FLAGS += -ggdb -Wall
#endif

# System-specific rules.

# Linux
ifeq ($(OS),Linux)
  M_FLAGS  += -shared -fPIC -lcurl
endif

# Macintosh
ifeq ($(OS),Darwin)
  INCS     += -I/sw/include
  LIBS     += -L/sw/lib
  M_FLAGS  += -dynamiclib -Xlinker -single_module
endif

# Solaris
ifeq ($(OS),SunOS)
  C_FLAGS  += 
  M_FLAGS  += -shared
endif

SRC     = *.c *.h

# External library dependences.
ifeq ($(BUILDTYPE),dist)
LIBS_vadi_mapper.so = `pkg-config --cflags --libs glib-2.0`
else
LIBS_vadi_mapper.so = `pkg-config --cflags --libs glib-2.0 gthread-2.0` -lcurl -lgssapi_krb5
endif

all: $(BIN_FILES) $(SO_FILES)
ifeq ($(BUILDTYPE),dist)
	@echo -e "\e[1;30mBuildtype was set as 'dist'.\e[0m"
endif

# Generic rules...

%.so:	%.c header.h module.h
	@echo Compiling \'$<\'.
	@$(CC) $(M_FLAGS) $(C_FLAGS) -o $@ $< $(LIBS_$@)
ifeq ($(BUILDTYPE),dist)
	@strip $@
endif

clean:
	@echo "Cleaning up."
	@rm -f *.o *.so $(BIN_FILES)

# Exceptions to these rules...

vadi_mapper.so: vadi_mapper.h

