#!/bin/sh

echo "Making bot."
cc -o mapper main.c /usr/local/lib/libjansson.a -lz -ldl `pkg-config --cflags --libs glib-2.0 gthread-2.0` -lcurl
make
echo "Done."
