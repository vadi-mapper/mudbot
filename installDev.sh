#!/bin/sh
apt-get install bzr bzr-gtk build-essential libcurl4-gnutls-dev libglib2.0-dev libjson-glib-dev p7zip-full
wget http://www.digip.org/jansson/releases/jansson-1.3.tar.bz2
bunzip2 -c jansson-1.3.tar.bz2 | tar xf -
cd jansson-1.3
./configure
make
make check
make install
CurDir=`pwd`
rm -rf $CurDir/jansson-1.3
rm $CurDir/jansson-1.3.tar.bz2
